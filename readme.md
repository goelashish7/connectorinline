# Sample Connector 
This contains the source for the GitHub Tab for Microsoft Teams.
 
#### Prerequisites
1. Download ngrok from https://ngrok.com/. Run the following command to setup a tunnel to localhost:3000
 `ngrok http 3000`
 Note the ngrok address, which looks something like `https://013e0d3f.ngrok.io`.
2. Use the ngrok all in manifest, connector portal and in the code.

### How to Run
 - install all the dependencies through npm install.
 - run node server.js.
